# Ultimate Slider Theme

Ultimate Slider Theme is a Drupal theme that features a full-width top slider, a
gallery on the home page, and captivating HTML plus JS animations. The theme
sets the tone with background images, creating an attractive web presence that
is sure to engage visitors.

## Table of Contents

- [Description](#description)
- [Features](#features)
- [Requirements](#requirements)
- [Installation](#installation)
- [Project Page](#project-page)
- [Bug Reports and Feature Suggestions](#bug-reports-and-feature-suggestions)
- [Maintainer](#maintainer)

## Description

Ultimate Slider Theme is developed using the latest technologies for Drupal 8
and 9. It utilizes stable modules, Font Awesome, and dark-awesome JS to enhance
the theme's functionality and design.

## Features

- Responsive, Mobile-Friendly Theme.
- In-built Font Awesome.
- Mobile support (Smartphone, Tablet, Android, iPhone, etc.)
- 8 block regions available.
- Minimalist main menu placed below the slider.
- Social media options in the footer for quick and convenient sharing.

## Requirements

This theme requires no modules outside of Drupal core.

## Installation

To install the Ultimate Slider Theme, follow these steps:

1. Clone the repository: `git clone https://github.com/your/repository.git`
2. Move the theme directory to your Drupal installation:
   `mv ultimate_slider_theme /path/to/drupal/themes/`
3. Log in to your Drupal site as an administrator.
4. Go to the Appearance page (`/admin/appearance`).
5. Find the Ultimate Slider Theme and click on the "Install and set as default"
   button.
6. Configure the theme settings according to your preferences.

## Project Page

For a full description of the theme, visit the [project page](https://www.drupal.org/project/fashion_beauty).

## Bug Reports and Feature Suggestions

To submit bug reports and feature suggestions, or to track changes, visit the
[issue tracker](https://www.drupal.org/project/issues/fashion_beauty).

## Maintainer

- [Arti Parmar](https://www.drupal.org/u/arti_parmar). Please reach out to Arti
  for any questions or support related to the Ultimate Slider Theme.
