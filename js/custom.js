
jQuery(document).ready(function(){
  jQuery("header ul li .caret").click(function(){
    jQuery(this).toggleClass("rotate")
    jQuery(this).next().toggleClass("show-menu");
  });
});

let slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  let i;
  let slides = document.getElementsByClassName("mySlides");
  let dots = document.getElementsByClassName("demo");
  let captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  if (captionText !== null) {
    captionText.innerHTML = dots[slideIndex-1].alt;
  }
  captionText.innerHTML = dots[slideIndex-1].alt;
}